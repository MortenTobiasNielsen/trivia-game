# Trivia Game

Trivia Game is an example project that shows some Vue fundamentals used to create a simple Trivia Game.

You can interact with the example here: TODO: Insert link to Heroku

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have visual studio code installed (or similar text editor/IDE)
- You have node and npm installed

## Installing Trivia Game

To install Trivia Game, follow these steps:

Open git bash and navigate to the desired location

Paste:
`git clone git@gitlab.com:MortenTobiasNielsen/trivia-game.git`

`cd trivia-game`

`npm install`

## Using Trivia Game

To use Trivia Game, follow these steps:

Open the solution in your IDE and run the project.

A websie will open up and you will be able to:

1. Select the amount of questions, the category, the difficulty, and then start the game
2. You will be presented with either multiple choice or true/false questions
3. After you have answered all the questions you will be presented with a result screen

## Contributing to Trivia Game

To contribute to Trivia Game, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin https://gitlab.com/MortenTobiasNielsen/trivia-game/`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

You name will go here if you contribute. :)

## Contact

Please make an issue with your request.

## License

This project uses the following license: [MIT](https://choosealicense.com/licenses/mit/).
